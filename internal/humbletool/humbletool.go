package humbletool

import (
	"bytes"
	cmd5 "crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

// readDir zwraca nazwy katalogów w przekazanym katalogu.
func readDir(name string) []string {
	files, err := os.ReadDir(name)
	if err != nil {
		log.Fatal(err)
	}
	dirs := make([]string, 0)
	for _, file := range files {
		if file.IsDir() {
			dirs = append(dirs, file.Name())
		}
	}
	return dirs
}

// readMd5 odczytuje sumy kontrolne wraz z odpowiadającymi im
// nazwami plików z pliku o podanej nazwie
func readMd5(name string) (map[string][16]byte, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, fmt.Errorf("błąd odczytu pliku z sumami kontrolnymi: %v", err)
	}

	lines := bytes.Split(data, []byte("\n"))
	//lines := bytes.Fields(data)
	filesWithMd5 := make(map[string][16]byte)
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		tmp := bytes.Split(line, []byte("  "))
		dst := make([]byte, 16)
		n, err := hex.Decode(dst, tmp[0])
		if err != nil || n < 16 {
			return nil, fmt.Errorf("błąd konwersji sumy kontrolnej(string na []byte): %v", err)
		}
		var md5 [16]byte
		for i, b := range dst {
			md5[i] = b
		}

		filesWithMd5[string(tmp[1])] = md5
	}
	return filesWithMd5, nil
}

// checkMd5 sprawdza zgodność sumy kontrolnej podanego pliku w wybranej lokalizacji.
func checkMd5(dir string, file string, md5 [16]byte) (bool, error) {
	path := filepath.Join(dir, file)
	f, err := os.Open(path)
	if err != nil {
		return false, fmt.Errorf("sprawdzanie sumy kontrolnej pliku %s: %v", path, err)

	}
	defer f.Close()

	h := cmd5.New()
	if _, err := io.Copy(h, f); err != nil {
		return false, fmt.Errorf("sprawdzanie sumy kontrolnej pliku %s: %v", path, err)

	}
	sum := h.Sum(nil)
	sumOK := true
	for i, b := range sum {
		if md5[i] != b {
			sumOK = false
			break
		}
	}
	if sumOK {
		return true, nil
	}
	return false, nil
}

// checkAllMd5InDir sprawdza sumy kontrolne odczytane z pliku md5 w podanym katalogu.
// Po wyliczeniu błędnej sumy kontrolnej przerywane jest dalsze sprawdzanie.
func checkAllMd5InDir(dir string) (map[string]interface{}, error) {
	md5Filename := filepath.Base(dir) + ".md5"
	md5s, err := readMd5(filepath.Join(dir, md5Filename))
	if err != nil {
		return nil, fmt.Errorf("błąd sprawdzenia sumy kontrolnej w katalogu %s: %v", dir, err)
	}

	result := make(map[string]interface{})
	for file, sum := range md5s {
		ok, err := checkMd5(dir, file, sum)
		if err != nil {
			result[file] = err
		} else {
			result[file] = ok
		}
	}
	return result, nil
}

// checkAllMd5InRootDir sprawdza sumy kontrolne we wszystkich podkatalogach
// podanego katalogu.
func checkAllMd5InRootDir(dir string) map[string]interface{} {
	result := make(map[string]interface{})
	dirs := readDir(dir)
	for _, d := range dirs {
		path := filepath.Join(dir, d)
		res, err := checkAllMd5InDir(path)
		if err != nil {
			result[d] = err
		} else {
			result[d] = res
		}
	}
	return result
}
