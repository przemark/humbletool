package humbletool

import (
	"path/filepath"
	"testing"
)

func equal(x, y []string) bool {
	if len(x) != len(y) {
		return false
	}
	for i := range x {
		if x[i] != y[i] {
			return false
		}
	}
	return true
}

func equalMap(x, y map[string][16]byte) bool {
	if len(x) != len(y) {
		return false
	}
	for k, xv := range x {
		if yv, ok := y[k]; !ok || yv != xv {
			return false
		}
	}
	return true
}

func TestReadDir(t *testing.T) {
	want := []string{"dir-1", "dir-2", "dir-3"}
	got := readDir("testdata/sample")
	if !equal(got, want) {
		t.Errorf("readDir(testdata/sample) == %s, want [\"dir-1\", \"dir-2\", \"dir-3\"]", got)
	}

}

func TestReadMd5(t *testing.T) {
	want := map[string][16]byte{
		"file-1.txt": [...]byte{0xa2, 0x3f, 0x1c, 0x37, 0x98, 0x29, 0xd7, 0x44, 0xeb, 0x85, 0x7b, 0x57, 0x44, 0x00, 0x8a, 0x31},
		"file-2.txt": [...]byte{0xc3, 0x32, 0x46, 0xd3, 0xdb, 0x22, 0x61, 0x29, 0xba, 0x39, 0x14, 0x3b, 0x64, 0x29, 0x89, 0x3d},
	}
	file := "testdata/sample/dir-1/dir-1.md5"
	got, _ := readMd5(file)
	if !equalMap(got, want) {
		t.Errorf("readMd5(%s) == %v", file, got)
	}
}

func TestCheckMd5(t *testing.T) {
	tests := map[string][16]byte{
		"file-1.txt": [...]byte{0xa2, 0x3f, 0x1c, 0x37, 0x98, 0x29, 0xd7, 0x44, 0xeb, 0x85, 0x7b, 0x57, 0x44, 0x00, 0x8a, 0x31},
		"file-2.txt": [...]byte{0xc3, 0x32, 0x46, 0xd3, 0xdb, 0x22, 0x61, 0x29, 0xba, 0x39, 0x14, 0x3b, 0x64, 0x29, 0x89, 0x3d},
	}

	dir := filepath.Join("testdata", "sample", "dir-1")
	file := "file-2.txt"
	ok, _ := checkMd5(dir, file, tests[file])
	if !ok {
		t.Errorf("checkMd5(%s,%s,%x) == %t", dir, file, tests[file], ok)
	}
}

func TestCheckAllMd5InDir(t *testing.T) {
	dir := filepath.Join("testdata", "sample", "dir-3")
	got, _ := checkAllMd5InDir(dir)
loop:
	for _, v := range got {
		switch v := v.(type) {
		case error:
			t.Errorf("checkAllMd5InDir(%s) == %t", dir, v)
			break loop
		}
	}
}

func TestCheckAllMd5InRootDir(t *testing.T) {
	dir := filepath.Join("testdata", "sample")
	got := checkAllMd5InRootDir(dir)
loop:
	for _, v := range got {
		switch v := v.(type) {
		case error:
			t.Errorf("checkAllMd5InRootDir(%s) == %t", dir, v)
			break loop
		}
	}
}

func TestCheckAllMd5InRootDirBadMd5(t *testing.T) {
	dir := filepath.Join("testdata", "badmd5")
	got := checkAllMd5InRootDir(dir)

	dmap := got["dir-1"]
	res, _ := dmap.(map[string]interface{})
	if res["file-2.txt"].(bool) {
		t.Errorf("checkAllMd5InRootDirBadMd5(%s) == %v", dir, got)
	}
}
