package humbletool

import (
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestReadDirM(t *testing.T) {
	want := []string{"dir-1", "dir-2", "dir-3"}
	got := readDir("testdata/sample")
	if !equal(got, want) {
		t.Errorf("readDir(testdata/sample) == %s, want [\"dir-1\", \"dir-2\", \"dir-3\"]", got)
	}
}

func TestReadMd5M(t *testing.T) {
	want := map[string][16]byte{
		"file-1.txt": [...]byte{0xa2, 0x3f, 0x1c, 0x37, 0x98, 0x29, 0xd7, 0x44, 0xeb, 0x85, 0x7b, 0x57, 0x44, 0x00, 0x8a, 0x31},
		"file-2.txt": [...]byte{0xc3, 0x32, 0x46, 0xd3, 0xdb, 0x22, 0x61, 0x29, 0xba, 0x39, 0x14, 0x3b, 0x64, 0x29, 0x89, 0x3d},
	}
	file := "testdata/sample/dir-1/dir-1.md5"
	got, _ := readMd5(file)
	if !equalMap(got, want) {
		t.Errorf("readMd5(%s) == %v", file, got)
	}
}

func TestCheckMd5M(t *testing.T) {
	tests := map[string][16]byte{
		"file-1.txt": [...]byte{0xa2, 0x3f, 0x1c, 0x37, 0x98, 0x29, 0xd7, 0x44, 0xeb, 0x85, 0x7b, 0x57, 0x44, 0x00, 0x8a, 0x31},
		"file-2.txt": [...]byte{0xc3, 0x32, 0x46, 0xd3, 0xdb, 0x22, 0x61, 0x29, 0xba, 0x39, 0x14, 0x3b, 0x64, 0x29, 0x89, 0x3d},
	}

	dir := filepath.Join("testdata", "sample", "dir-1")
	file := "file-2.txt"
	ch := make(chan md5Res)
	go checkMd5M(ch, dir, file, tests[file])
	got := <-ch

	if !got.Ok {
		t.Errorf("checkMd5(%s,%s,%x) == %t", dir, file, tests[file], got.Ok)
	}
}

func TestCheckAllMd5InDirM(t *testing.T) {
	dir := filepath.Join("testdata", "sample", "dir-3")
	ch := make(chan md5sRes)
	go checkAllMd5InDirM(ch, dir)
	got := <-ch
	if got.Err != nil {
		t.Errorf("checkAllMd5InDir(%s) == %v", dir, got)
		return
	}
	for _, v := range got.Md5s {
		if v.Err != nil || !v.Ok {
			t.Errorf("checkAllMd5InDir(%s) == %v", dir, got)
			break
		}
	}
}

func TestCheckAllMd5InRootDirM(t *testing.T) {
	dir := filepath.Join("testdata", "sample")
	got := CheckAllMd5InRootDirM(dir)
loop:
	for _, v := range got {
		if v.Err != nil {
			t.Errorf("checkAllMd5InRootDir(%s) == %v", dir, got)
			break
		}
		for _, item := range v.Md5s {
			if item.Err != nil || !item.Ok {
				t.Errorf("checkAllMd5InRootDir(%s) == %v", dir, got)
				break loop
			}
		}
	}
}

func TestCheckAllMd5InRootDirBadMd5M(t *testing.T) {
	dir := filepath.Join("testdata", "badmd5")
	got := CheckAllMd5InRootDirM(dir)

	if got[0].Dir != "dir-1" {
		t.Errorf("checkAllMd5InRootDirBadMd5(%s) == %v", dir, got)

	}
	for _, v := range got[0].Md5s {
		if (v.File == "file-1.txt" && !v.Ok) || (v.File == "file-2.txt" && v.Ok) {
			t.Errorf("checkAllMd5InRootDirBadMd5(%s) == %v", dir, got)
		}
	}
}

func TestSaveToFile(t *testing.T) {
	dir := filepath.Join("testdata", "sample")
	log := filepath.Join("testdata", "log", "humble_bundle.log")
	os.Remove("testdata/log/humble_bundle.log")
	res := CheckAllMd5InRootDirM(dir)
	SaveToFile(log, res)
	data, _ := os.ReadFile(log)
	got := strings.Contains(string(data), "Status(sprawdzone/poprawne): 3/3")
	if !got {
		t.Errorf("saveToFile(%s) == %v", log, got)
	}
	os.Remove("testdata/log/humble_bundle.log")
}

func TestSaveToFileErrorNoneFile(t *testing.T) {
	dir := filepath.Join("testdata", "error")
	log := filepath.Join("testdata", "log", "humble_bundle.log")
	os.Remove("testdata/log/humble_bundle.log")
	res := CheckAllMd5InRootDirM(dir)
	SaveToFile(log, res)
	data, _ := os.ReadFile(log)
	got := strings.Contains(string(data), "Status(sprawdzone/poprawne): 1/0")
	if !got {
		t.Errorf("saveToFileErrorNoneFile(%s) == %v", log, got)
	}
	os.Remove("testdata/log/humble_bundle.log")
}

func TestSaveToFileErrorNoneMd5File(t *testing.T) {
	dir := filepath.Join("testdata", "error_none_md5_file")
	log := filepath.Join("testdata", "log", "humble_bundle.log")
	os.Remove("testdata/log/humble_bundle.log")
	res := CheckAllMd5InRootDirM(dir)
	SaveToFile(log, res)
	data, _ := os.ReadFile(log)
	got := strings.Contains(string(data), "Status(sprawdzone/poprawne): 2/1")
	if !got {
		t.Errorf("saveToFileErrorNoneFile(%s) == %v", log, got)
	}
	os.Remove("testdata/log/humble_bundle.log")
}
