package humbletool

import (
	cmd5 "crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"text/template"
	"time"
)

type md5Res struct {
	File string
	Err  error
	Ok   bool
}

type md5sRes struct {
	Dir  string
	Md5s []md5Res
	Err  error
}

var LimitGoroutinesInChechMd5 = 4
var tokensMd5 = make(chan struct{}, LimitGoroutinesInChechMd5)

var LimitGoroutinesInDirCheck = 10
var tokensInDirCheck = make(chan struct{}, LimitGoroutinesInDirCheck)

// checkMd5 sprawdza zgodność sumy kontrolnej podanego pliku w wybranej lokalizacji.
func checkMd5M(ch chan<- md5Res, dir, file string, md5 [16]byte) {
	tokensMd5 <- struct{}{}
	defer func() { <-tokensMd5 }()

	path := filepath.Join(dir, file)
	f, err := os.Open(path)
	if err != nil {
		ch <- md5Res{
			File: file,
			Err:  fmt.Errorf("sprawdzanie sumy kontrolnej pliku %s: %v", path, err),
		}
		return
	}
	defer f.Close()

	h := cmd5.New()
	if _, err := io.Copy(h, f); err != nil {
		ch <- md5Res{
			File: file,
			Err:  fmt.Errorf("sprawdzanie sumy kontrolnej pliku %s: %v", path, err),
		}
		return
	}
	sum := h.Sum(nil)
	sumOK := true
	for i, b := range sum {
		if md5[i] != b {
			sumOK = false
			break
		}
	}

	if sumOK {
		ch <- md5Res{
			File: file,
			Ok:   true,
		}
	} else {
		ch <- md5Res{
			File: file,
		}
	}
}

// checkAllMd5InDir sprawdza sumy kontrolne odczytane z pliku md5 w podanym katalogu.
func checkAllMd5InDirM(chMd5s chan<- md5sRes, dir string) {
	tokensInDirCheck <- struct{}{}
	defer func() { <-tokensInDirCheck }()

	md5Filename := filepath.Base(dir) + ".md5"
	md5s, err := readMd5(filepath.Join(dir, md5Filename))
	if err != nil {
		chMd5s <- md5sRes{
			Dir: filepath.Base(dir),
			Err: fmt.Errorf("błąd sprawdzenia sumy kontrolnej w katalogu %s: %v", dir, err),
		}
		return
	}

	res := make([]md5Res, 0, len(md5s))
	ch := make(chan md5Res)
	for file, sum := range md5s {
		go checkMd5M(ch, dir, file, sum)
	}
	for i := 0; i < len(md5s); i++ {
		r := <-ch
		res = append(res, r)
	}
	chMd5s <- md5sRes{
		Dir:  filepath.Base(dir),
		Md5s: res,
	}
}

// checkAllMd5InRootDir sprawdza sumy kontrolne we wszystkich podkatalogach
// podanego katalogu.
func CheckAllMd5InRootDirM(dir string) []md5sRes {
	dirs := readDir(dir)
	result := make([]md5sRes, 0, len(dirs))
	ch := make(chan md5sRes)
	for _, d := range dirs {
		path := filepath.Join(dir, d)
		fmt.Println("Sprawdzam:", d)
		go checkAllMd5InDirM(ch, path)
	}

	for range dirs {
		r := <-ch
		fmt.Println("Zakończono sprawdzanie:", r.Dir)
		result = append(result, r)
	}
	return result
}

func NumberOfOk(res []md5sRes) int {
	n := 0
loop:
	for _, v := range res {
		if v.Err != nil {
			continue
		}
		for _, item := range v.Md5s {
			if item.Err != nil || !item.Ok {
				continue loop
			}
		}
		n++
	}
	return n
}

func MaxFileNameSize(files []md5Res) int {
	max := 0
	for _, v := range files {
		lf := len(v.File)
		if lf > max {
			max = lf
		}
	}
	return max
}

// saveToFile zapisuje wynik sprawdzania sum kontrolnych do pliku
func SaveToFile(file string, res []md5sRes) {
	const templ = `
------------------------------------------------------
Data: {{.Date}}
{{range $dir := .Res}}
  {{$dir.Dir}}
    {{- if $dir.Err}}
	{{$dir.Err}}
	{{else}}
	{{range $file := $dir.Md5s}}
	  {{- if $file.Err}}{{$file.File}}   {{$file.Err}}{{else}}{{$s := maxFileNameSize $dir.Md5s | printf "%%-%ds  %%t"}}{{printf $s $file.File $file.Ok}}{{end}}
	{{end}}
	{{- end}}
{{- end}}
Status(sprawdzone/poprawne): {{len .Res}}/{{.Res | numberOfOk}}
------------------------------------------------------
	`

	type result struct {
		Date string
		Res  []md5sRes
	}

	report := template.Must(template.New("report").
		Funcs(template.FuncMap{"numberOfOk": NumberOfOk}).
		Funcs(template.FuncMap{"maxFileNameSize": MaxFileNameSize}).
		Parse(templ))
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Printf("zapis do pliku: %v", err)
	}
	defer f.Close()

	if err := report.Execute(f, result{time.Now().Format("02/01/2006 15:04:05"), res}); err != nil {
		log.Printf("zapis do pliku: %v", err)
	}
}
