package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/humbletool/internal/humbletool"
)

var (
	go1Flag = flag.Int("go1", 10, "limit of goroutines in check md5")
	go2Flag = flag.Int("go2", 20, "limit of goroutines in check dir")
	dirFlag = flag.String("dir", ".", "path to dir")
	logFlag = flag.String("log", "humble_tool.log", "path to log file")
)

func main() {
	fmt.Println("Humble Tool")
	flag.Parse()
	humbletool.LimitGoroutinesInChechMd5 = *go1Flag
	humbletool.LimitGoroutinesInChechMd5 = *go2Flag
	curr, _ := os.Getwd()
	fmt.Println("Bieżący katalog:", curr)

	result := humbletool.CheckAllMd5InRootDirM(*dirFlag)
	humbletool.SaveToFile(*logFlag, result)
	fmt.Printf("Status(sprawdzone/poprawne): %d/%d\n", len(result), humbletool.NumberOfOk(result))
}
